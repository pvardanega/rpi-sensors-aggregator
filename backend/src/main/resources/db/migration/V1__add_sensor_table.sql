create table sensors
(
	id uuid not null
		constraint sensors_pkey
			primary key,
	humidity double precision not null,
	measured_at timestamp not null,
	name varchar(255) not null,
	temperature double precision not null
);

create index on sensors (name);
create index on sensors (measured_at);