package ch.vardanega.rpisensorsaggregator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/sensors")
class SensorController {

    private final SensorRepository sensorRepository;

    @Autowired
    SensorController(SensorRepository sensorRepository) {
        this.sensorRepository = sensorRepository;
    }

    @GetMapping
    public List<Sensor> findLatestValue() {
        return sensorRepository.findLatestValue();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addNewMeasures(@RequestBody Sensor sensor) {
        sensorRepository.save(sensor);
    }

    @GetMapping("{name}")
    public List<Sensor> findAllMeasuresOfOneSensor(@PathVariable("name") String sensorName) {
        return sensorRepository.findAllByNameOrderByMeasuredAt(sensorName);
    }
}
