package ch.vardanega.rpisensorsaggregator;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
interface SensorRepository extends JpaRepository<Sensor, UUID> {

    @Query(
        nativeQuery = true,
        value = "SELECT * from sensors s1 where not exists (select * from sensors s2 where s2.measured_at > s1.measured_at and s2.name = s1.name)"
    )
    List<Sensor> findLatestValue();

    List<Sensor> findAllByNameOrderByMeasuredAt(String name);

}
