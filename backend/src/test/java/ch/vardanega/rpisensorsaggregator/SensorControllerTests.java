package ch.vardanega.rpisensorsaggregator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
@Sql(
    statements = {
        "delete from sensors;",
        "insert into sensors values ('1678CE40-8214-4D4C-A01F-FA0AE2792589', 53.4, '2019-09-30 22:10:49.000000', 'Chambre 1', 23.4);",
        "insert into sensors values ('B37F2833-30C3-4F34-BA78-751A5E022EE5', 56.1, '2019-09-30 22:10:49.000000', 'Chambre 2', 22.9);",
        "insert into sensors values ('4DE0F066-9037-40B1-9B61-EC579824FC74', 53.3, '2019-09-30 22:15:49.000000', 'Chambre 1', 23.5);",
        "insert into sensors values ('594DDC4A-BC81-47C6-AF74-854978568AD0', 56.2, '2019-09-30 22:15:49.000000', 'Chambre 2', 22.8);"
    }
)
public class SensorControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void should_get_last_sensors_value() throws Exception {
        mockMvc.perform(get("/sensors")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].name").value(contains("Chambre 1", "Chambre 2")))
            .andExpect(jsonPath("$.[*].humidity").value(contains(53.3, 56.2)))
            .andExpect(jsonPath("$.[*].temperature").value(contains(23.5, 22.8)));
    }

    @Test
    public void should_get_all_measures_of_one_sensor() throws Exception {
        mockMvc.perform(get("/sensors/Chambre 1")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].name").value(contains("Chambre 1", "Chambre 1")))
            .andExpect(jsonPath("$.[*].humidity").value(contains(53.4, 53.3)))
            .andExpect(jsonPath("$.[*].temperature").value(contains(23.4, 23.5)));
    }

    @Test
    public void should_add_one_new_measure() throws Exception {
        mockMvc.perform(post("/sensors")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\"name\":\"Chambre 1\", \"humidity\":53.35, \"temperature\":23.45}"))
            .andExpect(status().isCreated());

        mockMvc.perform(get("/sensors/Chambre 1")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*].name").value(contains("Chambre 1", "Chambre 1", "Chambre 1")))
            .andExpect(jsonPath("$.[*].humidity").value(contains(53.4, 53.3, 53.35)))
            .andExpect(jsonPath("$.[*].temperature").value(contains(23.4, 23.5, 23.45)));
    }
}
